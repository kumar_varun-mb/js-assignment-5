const counterFactory=require('../counterFactory.js');

console.log(counterFactory(5).increment());
console.log(counterFactory(4).increment());
console.log(counterFactory(3).decrement());
console.log(counterFactory(2).decrement());