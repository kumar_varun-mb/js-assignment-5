let ctr=1;
function limitFunctionCallCount(cb, n) {
    function inner(){
        if(ctr<=n){
            cb();
            ctr++;
        }
        else
            console.log(null);
    }
    return inner;
}
module.exports=limitFunctionCallCount;