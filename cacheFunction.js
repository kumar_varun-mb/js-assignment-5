function cacheFunction(cb) {
    let obj={};
    function inner(val){
        if(val in obj)
            return 'cached result for '+val+' -> '+obj[val];
        else{
            let temp=cb(val);
            obj[val]=temp;
            return 'cb invoked for '+val;
            }
    }
    return inner;
}
module.exports=cacheFunction;