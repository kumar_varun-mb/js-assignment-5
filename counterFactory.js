function counterFactory(ctr) {
    let obj= {
        increment: () =>{
            return ++ctr;
        },
        decrement: () =>{
            return --ctr;
        }
    };
    return obj;
}
module.exports = counterFactory;